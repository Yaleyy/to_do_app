import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:to_do_app_2/data/database.dart';
import 'package:to_do_app_2/widgets/to_do_tile.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final _myBox = Hive.box('mybox');

  ToDoDataBase db = ToDoDataBase();

  // List toDoList = [
  //   ['Установить приложение', true],
  //   // ['Сделать упражнение', false],
  // ];

  void checkBoxChanged(bool? value, int index) {
    setState(() {
        db.toDoList[index][1] = value;
      });
    db.updateDataBase();
  }

  TextEditingController myController = TextEditingController();
  void createNewTask() {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          backgroundColor: Colors.deepPurpleAccent,
          content: Container(
            height: 120,
            width: 200,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextField(
                  controller: myController,
                  decoration: InputDecoration(
                    hintText: 'Добавить задачу',
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.deepPurpleAccent.shade700
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Colors.deepPurpleAccent.shade700
                      ),
                    ),
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.deepPurpleAccent.shade700
                        ),
                        onPressed: () {
                          setState(() {
                            db.toDoList.add([myController.text, false]);
                            myController.clear();
                          });
                          Navigator.pop(context);
                          db.updateDataBase();
                        },
                        child: Text('Сохранить'),
                      ),
                    ),
                    const SizedBox(width: 5),
                    Expanded(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.deepPurpleAccent.shade700
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text('Отменить'),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  void initState() {
    if (_myBox.get('TODOLIST') == null){
      db.createInitialData();
    } else {
      db.loadData();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.deepPurpleAccent,
      appBar: AppBar(
        title: Text('СПИСОК ЗАДАЧ'),
      ),
      body: ListView.builder(
        itemCount: db.toDoList.length,
        itemBuilder: (context, index) {
          return ToDoTile(
            nameTask: db.toDoList[index][0],
            valueCheckbox: db.toDoList[index][1],
            onChanged: (value) => checkBoxChanged(value, index),
            onPressedIcon: () {
              setState(() {
                db.toDoList.removeAt(index);
              });
              db.updateDataBase();
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.deepPurpleAccent.shade700,
        onPressed: createNewTask,
        child: Icon(Icons.add),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class ToDoTile extends StatelessWidget {
  final String nameTask;
  final bool valueCheckbox;
  Function(bool?)? onChanged;
  Function() onPressedIcon;

  ToDoTile(
      {super.key,
      required this.nameTask,
      required this.valueCheckbox,
      required this.onChanged,
      required this.onPressedIcon
      });
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 25, left: 25, right: 25),
      child: Container(
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
          color: Colors.deepPurpleAccent.shade700,
          borderRadius: BorderRadius.circular(12),
        ),
        child: Row(
          children: [
            Checkbox(
              value: valueCheckbox,
              onChanged: onChanged,
              activeColor: Colors.black,
            ),
            Expanded(
              child: Text(
                '$nameTask',
                style: TextStyle(
                  decoration: valueCheckbox == false
                      ? TextDecoration.none
                      : TextDecoration.lineThrough,
                  fontSize: 15,
                ),
              ),
            ),
            if (valueCheckbox == true)
              Container(
                width: 30,
                height: 30,
                child: MaterialButton(
                  padding: EdgeInsets.all(0),
                  splashColor: Colors.white,
                  color: Colors.transparent,
                  elevation: 0,
                  onPressed: onPressedIcon,
                  child: Icon(
                    Icons.delete,
                    color: Colors.red,
                    size: 25,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

abstract class ThemeAppBar{
  static final style = AppBarTheme(
    backgroundColor: Colors.deepPurpleAccent.shade700,
    centerTitle: true,
    titleTextStyle: const TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.w500,
    ),
  );
}